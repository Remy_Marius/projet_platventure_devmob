package com.mygdx.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
//import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.ScreenUtils;

import org.graalvm.compiler.word.Word;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class MyGdxGame extends Game {

	GameScreen gameScreen;
	FirstScreen firstScreen;
	@Override

	public void create() {

		 	 firstScreen=new FirstScreen(this);
		 	 setScreen(firstScreen);
	}
	public void startGame(){
		gameScreen=new GameScreen(1, this, 0);
		setScreen(gameScreen);
	}
	public void nextLevel(int nbLevel, int pts){
		gameScreen=new GameScreen(nbLevel+1, this, pts);
		setScreen(gameScreen);
	}
	public void restart(int nbLevel){
		gameScreen=new GameScreen(nbLevel, this, 0);
		setScreen(gameScreen);
	}

	@Override
	public void dispose() {
		super.dispose();
		gameScreen.dispose();
	}

	@Override
	public void render() {
		Gdx.app.debug("oulaaa", "oula");
		super.render();
	}

	@Override
	public void resize(int width, int height) {
		if (gameScreen!=null)gameScreen.resize(width, height);
		else  firstScreen.resize(width, height);
	}
}
