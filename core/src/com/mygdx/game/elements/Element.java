package com.mygdx.game.elements;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;

import org.w3c.dom.Text;

public class Element {
    public String type="";
    public float x, y, height, width;
    Sprite sprite;
    Texture texture;
    public Element(float x, float y, int width, int height, Texture t, String type){
        this.x=x;
        this.y=y;
        this.height=height;
        this.width=width;
        this.texture=t;
        this.sprite=new Sprite(texture);
        this.type=type;
        //sprite.setScale(0.32f);

        //sprite.setPosition(x, y);
    }
    public Element(float x, float y,String type){
        this.x=x;
        this.y=y;
        this.type=type;
    }
    public Sprite getSprite(){
        return sprite;
    }
}
