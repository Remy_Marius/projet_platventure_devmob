package com.mygdx.game.elements;

import com.badlogic.gdx.physics.box2d.Body;

public class Joyau extends Element{
    private Body body;
    public Joyau(float x, float y, String type) {
        super(x, y, type);
    }

    public Body getBody() {
        return body;
    }

    public void setBody(Body body) {
        this.body = body;
    }
}
