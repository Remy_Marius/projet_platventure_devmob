package com.mygdx.game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.mygdx.game.elements.Element;
import com.mygdx.game.elements.Joyau;

import java.util.ArrayList;

public class GameScreen implements Screen {
    private boolean oneTouch;
    private boolean twoTouch;
    private boolean panneau=false;
    private Sprite sp;
    private TextureRegion textureRegion;
    private boolean perdu=false;
    private String txtResult="";
    private MyGdxGame game;
    private World world=new World(new Vector2(0, -10), false);
    private Box2DDebugRenderer b2dr= new Box2DDebugRenderer();
    private Joueur player;
    //private int score=0;
    private Stage stage;
    private Stage stageText;
    //world parameters
    private int screenWidth=Gdx.graphics.getWidth();
    private int screenHeight=Gdx.graphics.getHeight();



    //screen
    private OrthographicCamera camera= new OrthographicCamera(screenWidth,screenHeight);
    private OrthographicCamera cameraText= new OrthographicCamera(screenWidth,screenHeight);



    private Viewport viewPort;
    private Viewport viewPortText;


    //graphics
    private SpriteBatch batch;
    private Texture background;


    //timing
    private int backgroundOffset;



    private BitmapFont police= new BitmapFont();
    private Label.LabelStyle lblStyle=new Label.LabelStyle();
    private Label lblScore;

    private int score=0;
    private int timer;
    int countUpdate=0;
    private Level lvl;
    private int nbLevel;
    private boolean gagne;

    //textures d'elements

    private Texture textureBack, textureJoyau1, textureJoyau2;
    private ArrayList<Element> listeElements=new ArrayList<Element>();

    private Animation idleJoyau1Animation ,idleJoyau2Animation;
    private TextureRegion[] joyau1Frames, joyau2Frames;
    private float elapsed=0;
    private Sound alert ,collision, gem, loose, plouf, win;
    GameScreen(int nblvl, MyGdxGame g, int pts){
        alert=Gdx.audio.newSound(Gdx.files.internal("Donnees_Projet/sounds/alert.ogg"));
        collision=Gdx.audio.newSound(Gdx.files.internal("Donnees_Projet/sounds/collision.ogg"));
        gem=Gdx.audio.newSound(Gdx.files.internal("Donnees_Projet/sounds/gem.ogg"));
        loose=Gdx.audio.newSound(Gdx.files.internal("Donnees_Projet/sounds/loose.ogg"));
        plouf=Gdx.audio.newSound(Gdx.files.internal("Donnees_Projet/sounds/plouf.ogg"));
        win=Gdx.audio.newSound(Gdx.files.internal("Donnees_Projet/sounds/win.ogg"));





        textureJoyau1=new Texture(Gdx.files.internal("Donnees_Projet/images/Gem_1.png"));
        textureJoyau2=new Texture(Gdx.files.internal("Donnees_Projet/images/Gem_2.png"));

        TextureRegion tmpFrames[][] = TextureRegion.split(textureJoyau1,56,56);
        TextureRegion tmpFrames2[][] = TextureRegion.split(textureJoyau2,56,56);

        int index =0;
        joyau1Frames=new TextureRegion[6];
        joyau2Frames=new TextureRegion[6];

        for (int i = 0; i < 6; i++) {
            joyau1Frames[i]=tmpFrames[i][0];
            joyau2Frames[i]=tmpFrames2[i][0];

        }
        idleJoyau1Animation=new Animation(1f/10f, (Object[])joyau1Frames);
        idleJoyau2Animation=new Animation(1f/10f, (Object[])joyau2Frames);

        score=pts;
        game=g;
        this.nbLevel=nblvl;
        lvl = new Level("level_00"+nbLevel+".txt");

        camera.position.set(screenWidth/16,screenHeight,0);
        cameraText.position.set(screenWidth/16,screenHeight,0);
        camera.update();
        cameraText.update();
        viewPort= new StretchViewport(16,12, camera);
        viewPortText= new StretchViewport(16,12, cameraText);

        stage=new Stage(viewPort);
        stageText=new Stage(viewPortText);

        batch = new SpriteBatch();
        batch.getProjectionMatrix().setToOrtho2D(0f,0f,16f,12f);
        this.timer= lvl.getTime();
        textureBack = new Texture(Gdx.files.internal("Donnees_Projet/images/"+lvl.getImageFileName()));


        loadLevel(lvl);


        world.setContactListener(new ContactListener() {
            @Override
            public void beginContact(Contact contact) {

                if(contact.getFixtureB().getBody().getUserData().equals("Player") && contact.getFixtureA().getBody().getUserData().equals("Plateforme")){
                    player.setCanJump(true);
                    if(player.getBody().getLinearVelocity().y<-3f){
                        collision.play();
                        System.out.println("bruit");
                    }
                    //System.out.println(player.canJump());
                }
                if(contact.getFixtureA().getBody().getUserData().equals("Player") && contact.getFixtureB().getBody().getUserData().equals("Joyau1")
                || contact.getFixtureB().getBody().getUserData().equals("Player") && contact.getFixtureA().getBody().getUserData().equals("Joyau1"))
                {    System.out.println("Joyau1");
                    gem.play();
                    score++;
                    final Body toRemove;
                    if(contact.getFixtureB().getBody().getUserData().equals("Joyau1")){
                         toRemove=contact.getFixtureB().getBody();
                    }
                    else{
                        toRemove=contact.getFixtureA().getBody();
                    }

                    Gdx.app.postRunnable(new Runnable() {
                        @Override
                        public void run () {
                            world.destroyBody(toRemove);
                            listeElements.remove(getJoyauByBody(toRemove));
                        }
                    });


                }
                if(contact.getFixtureA().getBody().getUserData().equals("Player") && contact.getFixtureB().getBody().getUserData().equals("Joyau2")
                        || contact.getFixtureB().getBody().getUserData().equals("Player") && contact.getFixtureA().getBody().getUserData().equals("Joyau2"))
                {    System.out.println("Joyau2");
                    gem.play();


                    score++;
                    final Body toRemove;
                    if(contact.getFixtureB().getBody().getUserData().equals("Joyau2")){
                        toRemove=contact.getFixtureB().getBody();
                    }
                    else{
                        toRemove=contact.getFixtureA().getBody();
                    }

                    Gdx.app.postRunnable(new Runnable() {
                        @Override
                        public void run () {
                            world.destroyBody(toRemove);
                            listeElements.remove(getJoyauByBody(toRemove));
                        }
                    });
                }

                if(contact.getFixtureA().getBody().getUserData().equals("Player") && contact.getFixtureB().getBody().getUserData().equals("Water")
                        || contact.getFixtureB().getBody().getUserData().equals("Player") && contact.getFixtureA().getBody().getUserData().equals("Water")){
                   plouf.play();
                    defaite();

                }
            }

            @Override
            public void endContact(Contact contact) {
                if(contact.getFixtureA().getBody().getUserData().equals("Player") && contact.getFixtureB().getBody().getUserData().equals("Sortie")
                        || contact.getFixtureB().getBody().getUserData().equals("Player") && contact.getFixtureA().getBody().getUserData().equals("Sortie")){
                    System.out.println(player.getBody().getPosition().x);
                    if (player.getBody().getPosition().x>=15||player.getBody().getPosition().x<=1){
                        System.out.println("gagne");


                     victoire();
                    }

                }

                }

            @Override
            public void preSolve(Contact contact, Manifold oldManifold) {
            }

            @Override
            public void postSolve(Contact contact, ContactImpulse impulse) {
            }
        });


        ////////////////////////
        ///////////////////////
        ///////////////////////
        FreeTypeFontGenerator fGen=new FreeTypeFontGenerator(Gdx.files.internal("Donnees_Projet/fonts/Comic_Sans_MS_Bold.ttf"));
        FreeTypeFontGenerator.FreeTypeFontParameter fparams= new FreeTypeFontGenerator.FreeTypeFontParameter();
        fparams.size=20;
        fparams.color= Color.YELLOW;
        fparams.borderColor=Color.BLACK;
        fparams.borderWidth=3;
        police =fGen.generateFont(fparams);
        fGen.dispose();
    }

    private void loadLevel(Level lvl) {
        int y=0;

        for(String line: lvl.getMap()){
            for (int i=0; i<line.length(); i++){

                char letter=line.charAt(i);

                if(letter>= 'A' &&  letter<= 'I'){
                    createBrick(world, i, lvl.getHeight()-y);

                    Texture t=new Texture("Donnees_Projet/images/Brick_"+letter+".png");
                    Element elt= new Element(i,y,Gdx.graphics.getWidth()/16,Gdx.graphics.getWidth()/16,t, "Brick");
                    listeElements.add(elt);

                }
                if(letter=='K'){
                    createPlateformCenter(world, i, lvl.getHeight()-y);
                    Texture t=new Texture("Donnees_Projet/images/Platform_"+letter+".png");
                    Element elt= new Element(i,y,Gdx.graphics.getWidth()/16,Gdx.graphics.getWidth()/16,t, "Platform");
                    listeElements.add(elt);
                }
                if(letter=='J'){
                    createPlateformLeft(world, i, lvl.getHeight()-y);
                    Texture t=new Texture("Donnees_Projet/images/Platform_"+letter+".png");
                    Element elt= new Element(i,y,Gdx.graphics.getWidth()/16,Gdx.graphics.getWidth()/16,t, "Platform");
                    listeElements.add(elt);
                }
                if(letter=='L'){
                    createPlateformRight(world, i, lvl.getHeight()-y);
                    Texture t=new Texture("Donnees_Projet/images/Platform_"+letter+".png");
                    Element elt= new Element(i,y,Gdx.graphics.getWidth()/16,Gdx.graphics.getWidth()/16,t, "Platform");
                    listeElements.add(elt);
                }
                if(letter=='W'){
                    Texture t=new Texture("Donnees_Projet/images/Water.png");
                    Element elt= new Element(i,y,Gdx.graphics.getWidth()/16,Gdx.graphics.getWidth()/16,t, "Water");
                    listeElements.add(elt);
                    createWater(world, i, lvl.getHeight()-y);
                }
                if(letter=='Z'){
                    Texture t=new Texture("Donnees_Projet/images/Exit_Z.png");
                    Element elt= new Element(i,y,Gdx.graphics.getWidth()/16,Gdx.graphics.getWidth()/16,t, "Brick");
                    listeElements.add(elt);
                    createSortie(world, i, lvl.getHeight()-y);
                }
                if(letter=='1'){
                    Joyau elt= new Joyau(i,y,"Joyau1");

                    createJoyau1(world, i, lvl.getHeight()-y, elt);

                }
                if(letter=='2'){
                    Joyau elt= new Joyau(i,y,"Joyau2");

                    createJoyau2(world, i, lvl.getHeight()-y, elt);
                }
                if(letter=='P'){
                    Texture t=new Texture("Donnees_Projet/images/Idle__000.png");
                    Sprite sprite=new Sprite(t);
                    this.player = new Joueur(world, i, lvl.getHeight()-y, sprite);

                    player.getBody().setFixedRotation(true);
                }
            }
            y++;

        }
    }
    public Joyau getJoyauByBody(Body b){
        for (Element e: listeElements) {
            if (e instanceof Joyau){
                if (((Joyau) e).getBody().equals(b)){
                    return (Joyau) e;
                }
            }
        }
        return null;
    }
    public void createJoyau1(World world, int x, int y, Joyau j){
       // System.out.println(x+","+y);
        BodyDef bdef= new BodyDef();
        FixtureDef fxdef1=new FixtureDef();
        CircleShape shape=new CircleShape();
        bdef.type=BodyDef.BodyType.StaticBody;
        bdef.position.set(x,y);
        Body body=world.createBody(bdef);

        shape.setRadius(0.25f);
        fxdef1.shape=shape;
        fxdef1.density=1;
        fxdef1.restitution=0.1f;
        fxdef1.isSensor=true;
        body.setUserData("Joyau1");
        body.createFixture(fxdef1);
        j.setBody(body);
        listeElements.add(j);
        shape.dispose();
    }
    public void createJoyau2(World world, int x, int y, Joyau j){
       // System.out.println(x+","+y);
        BodyDef bdef= new BodyDef();
        FixtureDef fxdef1=new FixtureDef();
        CircleShape shape=new CircleShape();
        bdef.type=BodyDef.BodyType.StaticBody;
        bdef.position.set(x,y);
        Body body=world.createBody(bdef);
        shape.setRadius(0.25f);
        fxdef1.shape=shape;
        fxdef1.density=1;
        fxdef1.restitution=0.1f;
        fxdef1.isSensor=true;
        body.setUserData("Joyau2");
        body.createFixture(fxdef1);
        j.setBody(body);
        listeElements.add(j);
        shape.dispose();
    }
    public void createSortie(World world, int x, int y){
       // System.out.println(x+","+y);
        BodyDef bdef= new BodyDef();
        FixtureDef fxdef1=new FixtureDef();
        PolygonShape shape=new PolygonShape();
        bdef.type=BodyDef.BodyType.StaticBody;
        bdef.position.set(x,y);
        Body body=world.createBody(bdef);
        shape.setAsBox(0.5f, 0.5f);
        fxdef1.shape=shape;
        fxdef1.density=1;
        fxdef1.restitution=0.1f;
        fxdef1.isSensor=true;
        body.setUserData("Sortie");
        body.createFixture(fxdef1);
        shape.dispose();
    }
    public void createWater(World world, int x, int y){
       // System.out.println(x+","+y);
        BodyDef bdef= new BodyDef();
        FixtureDef fxdef1=new FixtureDef();
        PolygonShape shape=new PolygonShape();
        bdef.type=BodyDef.BodyType.StaticBody;
        bdef.position.set(x,y);
        Body body=world.createBody(bdef);
        shape.setAsBox(0.5f, 0.375f);
        fxdef1.shape=shape;
        fxdef1.density=1;
        fxdef1.restitution=0.1f;
        fxdef1.isSensor=true;
        body.setUserData("Water");
        body.createFixture(fxdef1);
        shape.dispose();
    }
    public void createPlateformLeft(World world, int x, int y){
       // System.out.println(x+","+y);
        BodyDef bdef= new BodyDef();
        FixtureDef fxdef1=new FixtureDef();
        PolygonShape shape=new PolygonShape();
        bdef.type=BodyDef.BodyType.StaticBody;
        bdef.position.set(x,y);
        Body body=world.createBody(bdef);
        float[] vertices = { -0.5f,0.375f, 0.5f,0.375f, 0.5f, -0.375f, 0f, -0.375f, -0.5f, 0};
        shape.set(vertices);
        fxdef1.shape=shape;
        fxdef1.density=1;
        fxdef1.restitution=0.1f;
        body.setUserData("Plateforme");

        body.createFixture(fxdef1);
        shape.dispose();
    }
    public void createPlateformRight(World world, int x, int y){
      //  System.out.println(x+","+y);
        BodyDef bdef= new BodyDef();
        FixtureDef fxdef1=new FixtureDef();
        PolygonShape shape=new PolygonShape();
        bdef.type=BodyDef.BodyType.StaticBody;
        bdef.position.set(x,y);
        Body body=world.createBody(bdef);
        float[] vertices = { 0.5f,0.375f, -0.5f,0.375f, -0.5f, -0.375f, 0f, -0.375f, 0.5f, 0};
        shape.set(vertices);
        fxdef1.shape=shape;
        fxdef1.density=1;
        fxdef1.restitution=0.1f;
        body.setUserData("Plateforme");
        body.createFixture(fxdef1);
        shape.dispose();
    }
    public void createPlateformCenter(World world, int x, int y){
       // System.out.println(x+","+y);
        BodyDef bdef= new BodyDef();
        FixtureDef fxdef1=new FixtureDef();
        PolygonShape shape=new PolygonShape();
        bdef.type=BodyDef.BodyType.StaticBody;
        bdef.position.set(x,y);
        Body body=world.createBody(bdef);
        shape.setAsBox(0.5f, 0.375f);
        fxdef1.shape=shape;
        fxdef1.density=1;
        fxdef1.restitution=0.1f;
        body.setUserData("Plateforme");

        body.createFixture(fxdef1);
        shape.dispose();
    }
    public void createBrick(World world, int x, int y){
       // System.out.println(x+","+y);
        BodyDef bdef= new BodyDef();
        FixtureDef fxdef1=new FixtureDef();
        PolygonShape shape=new PolygonShape();
        bdef.type=BodyDef.BodyType.StaticBody;
        bdef.position.set(x,y);
        Body body=world.createBody(bdef);
        shape.setAsBox(0.5f, 0.5f);
        fxdef1.shape=shape;
        fxdef1.density=1;
        fxdef1.restitution=0.1f;
        body.setUserData("Plateforme");

        body.createFixture(fxdef1);
        shape.dispose();
    }


    public void defaite(){
        loose.play();
        txtResult="Dommage :-/";
        perdu=true;
        countUpdate=0;
    }
    public void victoire(){
            win.play();
            txtResult="Bravo :-)";
            gagne=true;
            countUpdate=0;
        }


    @Override
    public void show() {

    }

    public void handleInput(float dt){
        if(Gdx.input.isKeyJustPressed(Input.Keys.UP)){
           // System.out.println("appuy");
            if(player.canJump()){
                Texture t=new Texture("Donnees_Projet/images/Jump__006.png");
                Sprite sprite=new Sprite(t);
                player.setSprite(sprite);

                this.player.getBody().applyForceToCenter(new Vector2(0f, 50), true);
                player.setCanJump(false);
            }


        }
        if(Gdx.input.isKeyJustPressed(Input.Keys.LEFT)||Gdx.input.isKeyPressed(Input.Keys.RIGHT)){
            Texture t=new Texture("Donnees_Projet/images/Run__003.png");
            Sprite sprite=new Sprite(t);
            player.setSprite(sprite);
        }
        if(Gdx.input.isKeyPressed(Input.Keys.LEFT)&&player.getBody().getLinearVelocity().x>=-1){
            if(player.canJump()){

                this.player.getBody().applyForceToCenter(new Vector2(-1f, 0),  true);



            }
           // System.out.println("appuy");

        }

        if(Gdx.input.isKeyPressed(Input.Keys.RIGHT)&&player.getBody().getLinearVelocity().x<=1){
            if(player.canJump()){


                this.player.getBody().applyForceToCenter(new Vector2(1f, 0),  true);

            }
        }
        if(Gdx.input.isTouched(0)){


                if (Gdx.input.getX() > Gdx.graphics.getWidth() / 2) {
                    if (player.canJump()) {
                        this.player.getBody().applyForceToCenter(new Vector2(1f, 0), true);
                    }
                } else {
                    if (player.canJump()) {
                        this.player.getBody().applyForceToCenter(new Vector2(-1f, 0), true);
                    }
                }
            }

        if(Gdx.input.isTouched(1)){
            if(player.canJump()){
                Texture t=new Texture("Donnees_Projet/images/Jump__006.png");
                Sprite sprite=new Sprite(t);
                player.setSprite(sprite);

                this.player.getBody().applyForceToCenter(new Vector2(0f, 50), true);
                player.setCanJump(false);
            }
        }


    }

    @Override
    public void render(float delta) {
        /*if(player.getBody().getPosition().y>lvl.getHeight()){
             camera.position.set(camera.position.x, player.getBody().getPosition().y, 0);
            camera.update();
            batch.getProjectionMatrix().setToOrtho2D(0, camera.position.y, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
            viewPort.update(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        }*/

        if(player.getBody().getLinearVelocity().y<0){
            Texture t=new Texture("Donnees_Projet/images/Jump__008.png");
            Sprite sprite=new Sprite(t);
            player.setSprite(sprite);
        }
        else if (player.getBody().getLinearVelocity().equals(new Vector2(0,0))){
            Texture t=new Texture("Donnees_Projet/images/Idle__000.png");
            Sprite sprite=new Sprite(t);
            player.setSprite(sprite);
        }
        if(player.getBody().getLinearVelocity().x<0){
            System.out.println(player.getSprite().getTexture().toString());
            if(!player.getSprite().isFlipX()){
                System.out.println("hihi");
                player.getSprite().setFlip(true, false);
            }

        }
        elapsed+=Gdx.graphics.getDeltaTime();
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        countUpdate++;
        String scoreText= "Score : "+score;
        handleInput(delta);
        textureRegion = new TextureRegion(textureBack);
        batch.begin();
            batch.draw(textureRegion, 5, 5, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
            police.setColor(Color.YELLOW);

        for (Element e: listeElements) {
           // e.getSprite().draw(batch);
            if(e.type.equals("Brick")){
                batch.draw(e.getSprite(),(Gdx.graphics.getWidth()/16f)*(e.x-0.5f),(Gdx.graphics.getHeight()/12f)*(lvl.getHeight()-e.y-0.5f),(Gdx.graphics.getWidth()/16),(Gdx.graphics.getHeight()/12));
            }
            else if(e.type.equals("Platform")){
                batch.draw(e.getSprite(),(Gdx.graphics.getWidth()/16f)*(e.x-0.5f),(Gdx.graphics.getHeight()/11.75f)*(lvl.getHeight()-e.y-0.5f),Gdx.graphics.getWidth()/16,Gdx.graphics.getHeight()/12*0.75f);

            }
            else if(e.type.equals("Water")){
                batch.draw(e.getSprite(),(Gdx.graphics.getWidth()/16f)*(e.x-0.5f),(Gdx.graphics.getHeight()/11.75f)*(lvl.getHeight()-e.y-0.5f),Gdx.graphics.getWidth()/16,Gdx.graphics.getHeight()/12*0.75f);

            }
            else if(e.type.equals("Joyau1")){
                // batch.draw(e.getSprite(),(Gdx.graphics.getWidth()/16f)*(e.x-0.5f),(Gdx.graphics.getHeight()/11.75f)*(lvl.getHeight()-e.y-0.5f),Gdx.graphics.getWidth()/16,Gdx.graphics.getHeight()/12*0.75f);
                batch.draw((TextureRegion)idleJoyau1Animation.getKeyFrame(elapsed, true),(Gdx.graphics.getWidth()/16f)*(e.x-0.25f),(Gdx.graphics.getHeight()/12f)*(lvl.getHeight()-e.y-0.25f) ,(Gdx.graphics.getWidth()/32),(Gdx.graphics.getHeight()/24));

            }
            else if(e.type.equals("Joyau2")){
                // batch.draw(e.getSprite(),(Gdx.graphics.getWidth()/16f)*(e.x-0.5f),(Gdx.graphics.getHeight()/11.75f)*(lvl.getHeight()-e.y-0.5f),Gdx.graphics.getWidth()/16,Gdx.graphics.getHeight()/12*0.75f);
                batch.draw((TextureRegion)idleJoyau2Animation.getKeyFrame(elapsed, true),(Gdx.graphics.getWidth()/16f)*(e.x-0.25f),(Gdx.graphics.getHeight()/12f)*(lvl.getHeight()-e.y-0.25f) ,(Gdx.graphics.getWidth()/32),(Gdx.graphics.getHeight()/24));

            }


        }
            batch.draw(player.getSprite(), (player.getBody().getPosition().x-0.25f)*(Gdx.graphics.getWidth()/16f-0.5f), (player.getBody().getPosition().y-0.40f)*(Gdx.graphics.getHeight()/12f), (Gdx.graphics.getWidth()/32), Gdx.graphics.getHeight()/12f);
            //sp.draw(batch);
            police.draw(batch, Integer.toString(timer), Gdx.graphics.getWidth()/2, Gdx.graphics.getHeight()-40, 10, Align.center, false);
            police.draw(batch, scoreText, Gdx.graphics.getWidth()-100, Gdx.graphics.getHeight()-40, 10, Align.center, false);
            police.draw(batch, txtResult, Gdx.graphics.getWidth()/2, Gdx.graphics.getHeight()/2, 10, Align.center, false);

        batch.end();
        world.step(1/60f,6, 1 );

        if(gagne){

            if(countUpdate==120){
                game.nextLevel(nbLevel, score);
            }
        }
        else if(perdu){

            if(countUpdate==120){
            game.restart(nbLevel);}
        }
        else  if(timer==0){
            defaite();
        }
        else{
            if(countUpdate==60&&timer>0){
                countUpdate=0;
                timer--;
            }
            if (player.getBody().getPosition().x>= lvl.getWidth()+0.5||player.getBody().getPosition().x<=-0.75){
                System.out.println("gagne");

                defaite();
            }
        }



        b2dr.render(world, camera.combined);
        //b2dr.render(world, cameraText.combined);
    }
    @Override
    public void resize(int width, int height) {
        viewPort.update(width, height);
        viewPortText.update(width, height);

        stage.getViewport().update(width, height, true);
        stage.getViewport().setScreenSize(width, height);
        batch.getProjectionMatrix().setToOrtho2D(0, 0, width, height);

        stage.draw();
        stage.act();
        camera.update();

        stageText.draw();
        stageText.act();
        cameraText.update();
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }


}
