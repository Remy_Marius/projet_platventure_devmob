package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;

import org.graalvm.compiler.loop.InductionVariable;

public class Joueur  {

    private float x, y;
    private int points;
    private Body body;
    private boolean canJump;
    private Sprite sprite;

    public Joueur(World world , int x, int y, Sprite sprite) {
        this.x = x;
        this.y = y;
        this.points = 0;
        this.body=createBodyPlayer(world,x,y);
        this.sprite=sprite;

    }
    private Body createBodyPlayer(World world, int x, int y){
        System.out.println(x+","+y);
        BodyDef bdef= new BodyDef();
        FixtureDef fxdef1=new FixtureDef();
        FixtureDef fxdef2=new FixtureDef();

        bdef.type=BodyDef.BodyType.DynamicBody;
        bdef.position.set(x,y);
        Body body=world.createBody(bdef);

        CircleShape circle=new CircleShape();
        circle.setRadius(0.125f);

        PolygonShape diamant=new PolygonShape();
        float[] vertices = {-0.25f, 0, 0, 0.5f, 0.25f, 0, 0, -0.25f};
        diamant.set(vertices);

        circle.setPosition(new Vector2(0, -0.25f));
        fxdef1.shape=diamant;
        fxdef1.density=0.5f;
        fxdef1.restitution=0.1f;
        fxdef1.friction=0.5f;
        fxdef2.shape=circle;
        fxdef2.density=0.5f;
        fxdef2.restitution=0.1f;
        fxdef2.friction=0.5f;

        body.createFixture(fxdef1);
        body.createFixture(fxdef2);
        body.setUserData("Player");
        diamant.dispose();
        circle.dispose();
        return body;


    }

    public float getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public Body getBody() {
        return body;
    }
    public Sprite getSprite(){
        return sprite;
    }

    public void setSprite(Sprite sprite) {
        this.sprite = sprite;
    }

    public void setBody(Body body) {
        this.body = body;
    }

    public boolean canJump() {
        return canJump;
    }

    public void setCanJump(boolean canJump) {
        this.canJump = canJump;
    }
}
