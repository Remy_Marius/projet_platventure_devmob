package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

public class FirstScreen implements Screen {
    private Texture texture;
    private TextureRegion textureRegion;
    private Sprite s;
    int countUpdate=0;
    MyGdxGame game;
    private int screenWidth=Gdx.graphics.getWidth();
    private int screenHeight=Gdx.graphics.getHeight();
    private OrthographicCamera camera= new OrthographicCamera(screenWidth,screenHeight);
    private Viewport viewPort;
    private SpriteBatch batch;
    private Stage stage;
    FirstScreen(MyGdxGame game){
        this.game=game;
        texture=new Texture(Gdx.files.internal("Donnees_Projet/images/Intro.png"));
        textureRegion=new TextureRegion(texture);
        Sound son =Gdx.audio.newSound(Gdx.files.internal("Donnees_Projet/sounds/win.ogg"));
        son.play();

        camera.position.set(screenWidth/16,screenHeight,0);
        camera.update();
        viewPort= new StretchViewport(16,12, camera);
        stage=new Stage(viewPort);

        batch = new SpriteBatch();
        batch.getProjectionMatrix().setToOrtho2D(0f,0f,16f,12f);

    }
    @Override
    public void show() {
    }

    @Override
    public void render(float delta) {
        countUpdate++;
        System.out.println(countUpdate);
       if (countUpdate==180) {
           System.out.println("la");
           game.startGame();
       }
       batch.begin();
            batch.draw(textureRegion, 5, 5, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());

        batch.end();

    }
    @Override
    public void resize(int width, int height) {
        viewPort.update(width, height);

        stage.getViewport().update(width, height, true);
        stage.getViewport().setScreenSize(width, height);
        batch.getProjectionMatrix().setToOrtho2D(0, 0, width, height);

        stage.draw();
        stage.act();
        camera.update();


    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
