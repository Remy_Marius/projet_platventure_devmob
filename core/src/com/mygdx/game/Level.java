package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;

import java.io.BufferedReader;

import java.io.IOException;

import java.util.ArrayList;


public class Level {
    private int height, width, time;

    private ArrayList<String> map = new ArrayList<>();
    private String imageFileName;
    private String backGround;
    public Level(String level) {
        this.imageFileName = imageFileName;


        FileHandle handle = Gdx.files.internal("Donnees_Projet/levels/"+level);
        try {
            BufferedReader br = handle.reader(100, "Big5");
            String line = br.readLine(); //on prend la premiere ligne
            String[] words_line1 = line.split(" "); //on sépare les mots

            this.width = Integer.parseInt(words_line1[0]); //on récupere la hauteur (premier mot)
            this.height = Integer.parseInt(words_line1[1]);//on récupere la hauteur (deuxieme mot)
            this.time = Integer.parseInt(words_line1[2]);//on récupere le temps (troisieme mot)
            line = br.readLine();//on lit la ligne suivante
            while (line!= null) {
                String oldLine=line;

                line = br.readLine();
                if(line==null){
                    this.imageFileName=oldLine;
                }
                else{
                    this.map.add(oldLine);
                    System.out.println(oldLine);
                }

            }
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }


    public String getImageFileName() {
        return imageFileName;
    }

    public void setImageFileName(String imageFileName) {
        this.imageFileName = imageFileName;
    }

    public ArrayList<String> getMap() {
        return map;
    }

    public void setMap(ArrayList<String> map) {
        this.map = map;
    }
}
